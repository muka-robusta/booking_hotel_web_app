package io.github.itsuprun.sample_webapp.controller;

import javax.servlet.http.HttpServletRequest;

public interface Command {

    String commandAction(HttpServletRequest request);

}
