package io.github.itsuprun.sample_webapp.controller;

import io.github.itsuprun.sample_webapp.domain.BookingApplication;
import io.github.itsuprun.sample_webapp.service.BookingApplicationService;
import io.github.itsuprun.sample_webapp.service.ReservationService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AdminPanelCommand implements Command {

    private final BookingApplicationService bookingApplicationService;
    private final ReservationService reservationService;
    private static final String CONTROLLER_PAGE = "admin/adminPanel.jsp";


    public AdminPanelCommand() {
        this.bookingApplicationService = new BookingApplicationService();
        this.reservationService = new ReservationService();
    }

    @Override
    public String commandAction(HttpServletRequest request) {
        final List<BookingApplication> allActiveApplications = bookingApplicationService.getAllActiveApplications();
        request.setAttribute("boookingApplication", allActiveApplications);
        return CONTROLLER_PAGE;
    }
}
