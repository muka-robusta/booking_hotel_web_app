package io.github.itsuprun.sample_webapp.controller;

import io.github.itsuprun.sample_webapp.controller.helper.ControllerHelper;
import io.github.itsuprun.sample_webapp.domain.Role;
import io.github.itsuprun.sample_webapp.domain.User;
import io.github.itsuprun.sample_webapp.dto.LoginDto;
import io.github.itsuprun.sample_webapp.service.UserService;

import javax.servlet.http.HttpServletRequest;

public class LoginCommand implements Command {

    private static final String LOGIN_PAGE = "login.jsp";
    private final UserService userService;
    public LoginCommand() {
        this.userService = new UserService();
    }

    @Override
    public String commandAction(HttpServletRequest request) {

        String[] validationFields = {"password", "email"};

        if (ControllerHelper.ifAllEmpty(request, validationFields))
            return LOGIN_PAGE;

        if (ControllerHelper.ifAnyEmpty(request, validationFields))
            return ControllerHelper.returnWithMessage("Check input for correctness", request, LOGIN_PAGE);

        final LoginDto loginDto = new LoginDto(request.getParameter("email"), request.getParameter("password"));
        User user = userService.checkLoginForCorrectPassword(loginDto);

        if (user == null)
            return ControllerHelper.returnWithMessage("Incorrect credentials", request, LOGIN_PAGE);

        request.getSession().setAttribute("user", user);

        if (user.getUserRole() == Role.ADMIN)
            return "admin/adminPanel.jsp";

        return "index.jsp";
    }

}
