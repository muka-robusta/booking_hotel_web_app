package io.github.itsuprun.sample_webapp.service;

import io.github.itsuprun.sample_webapp.database.DaoFactory;
import io.github.itsuprun.sample_webapp.database.UserDao;
import io.github.itsuprun.sample_webapp.domain.User;
import io.github.itsuprun.sample_webapp.dto.LoginDto;

import java.util.List;

public class UserService {


    private final DaoFactory daoFactory = DaoFactory.getInstance();

    public User checkLoginForCorrectPassword(LoginDto loginDto) {
        try (final UserDao userDao =  daoFactory.createUserDao()) {
            final User foundUser = userDao.findByEmail(loginDto.getEmail());
            if (foundUser.getPassword().equals(loginDto.getPassword()))
                return foundUser;
        }
        return null;
    }

    public List<User> getAllUsers() {
        try (UserDao userDao = daoFactory.createUserDao()) {
            return userDao.findAll();
        }

    }

}
