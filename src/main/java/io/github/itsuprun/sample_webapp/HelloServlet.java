package io.github.itsuprun.sample_webapp;

import io.github.itsuprun.sample_webapp.controller.AdminPanelCommand;
import io.github.itsuprun.sample_webapp.controller.BookingApplicationCommand;
import io.github.itsuprun.sample_webapp.controller.Command;
import io.github.itsuprun.sample_webapp.controller.LoginCommand;
import io.github.itsuprun.sample_webapp.service.BookingApplicationService;
import io.github.itsuprun.sample_webapp.service.ReservationService;
import io.github.itsuprun.sample_webapp.service.UserService;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class HelloServlet extends HttpServlet {

    private Map<String, Command> commands = new HashMap<>();

    public void init() {
        commands.put("login", new LoginCommand());
        commands.put("application", new BookingApplicationCommand());
        commands.put("adminpanel", new AdminPanelCommand());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        handleRequest(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        handleRequest(request, response);
    }

    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");

        final String requestURI = request.getRequestURI().replaceAll("/", "");
        System.out.println(String.format("Requested URI -> %s", requestURI));
//        System.out.println(commands.keySet());

        String returnPage = commands.get(requestURI).commandAction(request);
        request.getRequestDispatcher(returnPage).forward(request, response);
    }

}