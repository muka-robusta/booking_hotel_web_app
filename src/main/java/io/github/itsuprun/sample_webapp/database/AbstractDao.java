package io.github.itsuprun.sample_webapp.database;

import java.util.List;

public interface AbstractDao<T, ID> extends AutoCloseable{
    T findById(ID id);
    List<T> findAll();
    void create(T obj);
    void update(T obj);
    void deleteById(ID id);
    void close();
}
