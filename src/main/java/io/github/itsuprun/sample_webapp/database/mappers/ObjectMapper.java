package io.github.itsuprun.sample_webapp.database.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ObjectMapper<T> {
    T extract(ResultSet resultSet) throws SQLException;
}
