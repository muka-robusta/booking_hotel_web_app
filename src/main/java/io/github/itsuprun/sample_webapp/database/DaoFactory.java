package io.github.itsuprun.sample_webapp.database;

import io.github.itsuprun.sample_webapp.database.rel_impl.JDBCDaoFactory;

public abstract class DaoFactory {

    private static volatile DaoFactory daoFactory;

    public abstract UserDao createUserDao();
    public abstract BookingApplicationDao createBookingApplicationDao();
    // ...

    public static DaoFactory getInstance() {
        if (daoFactory == null) {
            synchronized (DaoFactory.class) {
                if (daoFactory == null) {
                    DaoFactory fact = new JDBCDaoFactory();
                    daoFactory = fact;
                }
            }
        }
        return daoFactory;
    }

}
