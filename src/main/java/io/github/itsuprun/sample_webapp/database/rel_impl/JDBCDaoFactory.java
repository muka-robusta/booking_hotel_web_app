package io.github.itsuprun.sample_webapp.database.rel_impl;

import io.github.itsuprun.sample_webapp.database.BookingApplicationDao;
import io.github.itsuprun.sample_webapp.database.DaoFactory;
import io.github.itsuprun.sample_webapp.database.UserDao;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class JDBCDaoFactory extends DaoFactory {

    private final DataSource dataSource = ConnectionPoolHolder.getDataSource();

    @Override
    public UserDao createUserDao() {
        return new JDBCUserDao(getConnection());
    }

    @Override
    public BookingApplicationDao createBookingApplicationDao() {
        return new JDBCBookingApplicationDao(getConnection());
    }

    private Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException throwables) {
            throw new RuntimeException(throwables);
        }
    }

}
