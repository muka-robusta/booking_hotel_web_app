package io.github.itsuprun.sample_webapp.database.rel_impl;

import io.github.itsuprun.sample_webapp.database.BookingApplicationDao;
import io.github.itsuprun.sample_webapp.database.mappers.BookingApplicationMapper;
import io.github.itsuprun.sample_webapp.domain.ApplicationStatus;
import io.github.itsuprun.sample_webapp.domain.BookingApplication;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCBookingApplicationDao implements BookingApplicationDao {

    private final Connection connection;

    public JDBCBookingApplicationDao(Connection connection) { this.connection = connection;};

    @Override
    public BookingApplication findById(Integer integer) {
        return null;
    }

    @Override
    public List<BookingApplication> findAll() {
        return new ArrayList<>();
    }

    @Override
    public void create(BookingApplication obj) {
        String preparedCreateStatementString = "INSERT INTO bookingapplication (number_of_beds, room_class, booking_date_start," +
                "booking_date_end, user_id, application_status) VALUES (?,?,?,?,?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(preparedCreateStatementString)) {
            preparedStatement.setInt(1, obj.getNumberOfBeds());
            preparedStatement.setString(2, obj.getRoomClass().toString());
            preparedStatement.setDate(3, Date.valueOf(obj.getBookingDateStart()));
            preparedStatement.setDate(4, Date.valueOf(obj.getBookingDateEnd()));
            preparedStatement.setInt(5, obj.getUser().getId());
            preparedStatement.setString(6, obj.getApplicationStatus().toString());

            if (preparedStatement.executeUpdate() == 0) {
                throw new RuntimeException("Nothing was added!");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void update(BookingApplication obj) {

    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public BookingApplication findByUser(Integer userId) {
        return null;
    }

    @Override
    public List<BookingApplication> findByStatus(ApplicationStatus applicationStatus) {
        String preparedStatementString = "Select * from bookingapplication " +
                "LEFT JOIN user on bookingapplication.user_id = user.id WHERE application_status = ?;";

        List<BookingApplication> bookingApplicationsList = new ArrayList<>();
        try (final PreparedStatement preparedStatement = connection.prepareStatement(preparedStatementString)) {

            preparedStatement.setString(1, "NEW");
            final ResultSet resultSet = preparedStatement.executeQuery();

            final BookingApplicationMapper bookingApplicationMapper = new BookingApplicationMapper();
            while (resultSet.next()) {
                bookingApplicationsList.add(bookingApplicationMapper.extract(resultSet));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return bookingApplicationsList;
    }
}
