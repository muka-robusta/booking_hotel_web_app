package io.github.itsuprun.sample_webapp.database;

import io.github.itsuprun.sample_webapp.domain.ApplicationStatus;
import io.github.itsuprun.sample_webapp.domain.BookingApplication;

import java.util.List;

public interface BookingApplicationDao extends AbstractDao<BookingApplication, Integer> {
    BookingApplication findByUser(Integer userId);
    List<BookingApplication> findByStatus (ApplicationStatus applicationStatus);
}