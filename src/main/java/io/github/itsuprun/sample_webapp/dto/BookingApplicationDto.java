package io.github.itsuprun.sample_webapp.dto;

import io.github.itsuprun.sample_webapp.domain.User;

public class BookingApplicationDto {
    private String numberOfBeds;
    private String roomClass;
    private String bookingStartDate;
    private String bookingEndDate;
    private User user;

    public BookingApplicationDto(String numberOfBeds, String roomClass, String bookingStartDate, String bookingEndDate, User user) {
        this.numberOfBeds = numberOfBeds;
        this.roomClass = roomClass;
        this.bookingStartDate = bookingStartDate;
        this.bookingEndDate = bookingEndDate;
        this.user = user;
    }

    public String getNumberOfBeds() {
        return numberOfBeds;
    }

    public void setNumberOfBeds(String numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    public String getRoomClass() {
        return roomClass;
    }

    public void setRoomClass(String roomClass) {
        this.roomClass = roomClass;
    }

    public String getBookingStartDate() {
        return bookingStartDate;
    }

    public void setBookingStartDate(String bookingStartDate) {
        this.bookingStartDate = bookingStartDate;
    }

    public String getBookingEndDate() {
        return bookingEndDate;
    }

    public void setBookingEndDate(String bookingEndDate) {
        this.bookingEndDate = bookingEndDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
