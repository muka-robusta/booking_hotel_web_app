package io.github.itsuprun.sample_webapp.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Reservations extends BaseEntity {

    // Variables

    private HotelRoom hotelRoom;
    private User user;
    private BigDecimal resultPrice;
    private String comment;
    private LocalDate localDateStart;
    private LocalDate localDateEnd;
    private BookingApplication bookingApplication;

    public BookingApplication getBookingApplication() {
        return bookingApplication;
    }

    public void setBookingApplication(BookingApplication bookingApplication) {
        this.bookingApplication = bookingApplication;
    }

    public LocalDate getLocalDateStart() {
        return localDateStart;
    }

    public void setLocalDateStart(LocalDate localDateStart) {
        this.localDateStart = localDateStart;
    }

    public LocalDate getLocalDateEnd() {
        return localDateEnd;
    }

    public void setLocalDateEnd(LocalDate localDateEnd) {
        this.localDateEnd = localDateEnd;
    }

    public HotelRoom getHotelRoom() {
        return hotelRoom;
    }

    public void setHotelRoom(HotelRoom hotelRoom) {
        this.hotelRoom = hotelRoom;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigDecimal getResultPrice() {
        return resultPrice;
    }

    public void setResultPrice(BigDecimal resultPrice) {
        this.resultPrice = resultPrice;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public static class Builder {
        private Reservations reservations;

        public Builder hotelRoom(HotelRoom hotelRoom) {
            reservations.setHotelRoom(hotelRoom);
            return this;
        }

        public Builder user(User user) {
            reservations.setUser(user);
            return this;
        }

        public Builder price(BigDecimal price) {
            reservations.setResultPrice(price);
            return this;
        }

        public Builder comment(String comment) {
            reservations.setComment(comment);
            return this;
        }

        public Builder startDate(LocalDate startDate) {
            reservations.setLocalDateStart(startDate);
            return this;
        }

        public Builder endDate(LocalDate endDate) {
            reservations.setLocalDateEnd(endDate);
            return this;
        }

        public Builder bookingApplication(BookingApplication bookingApplication) {
            reservations.setBookingApplication(bookingApplication);
            return this;
        }

        public Reservations build() {
            return reservations;
        }


    }

}
