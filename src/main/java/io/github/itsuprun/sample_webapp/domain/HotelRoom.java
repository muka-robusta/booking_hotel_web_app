package io.github.itsuprun.sample_webapp.domain;

import java.math.BigDecimal;

public class HotelRoom extends BaseEntity {

    private Integer roomNumber;
    private Integer numberOfBeds;
    private RoomClass roomClass;
    private BigDecimal price;

    public HotelRoom(Integer roomNumber, Integer numberOfBeds, RoomClass roomClass, BigDecimal price) {
        this.roomNumber = roomNumber;
        this.numberOfBeds = numberOfBeds;
        this.roomClass = roomClass;
        this.price = price;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getNumberOfBeds() {
        return numberOfBeds;
    }

    public void setNumberOfBeds(Integer numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    public RoomClass getRoomClass() {
        return roomClass;
    }

    public void setRoomClass(RoomClass roomClass) {
        this.roomClass = roomClass;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}

