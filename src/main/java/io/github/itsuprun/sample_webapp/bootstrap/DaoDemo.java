package io.github.itsuprun.sample_webapp.bootstrap;

import io.github.itsuprun.sample_webapp.database.DaoFactory;
import io.github.itsuprun.sample_webapp.database.UserDao;
import io.github.itsuprun.sample_webapp.domain.RoomClass;
import io.github.itsuprun.sample_webapp.domain.User;

import java.util.List;
import java.util.UUID;

public class DaoDemo {
    private final DaoFactory daoFactory = DaoFactory.getInstance();

    public static void main(String[] args) {
        DaoDemo daoDemo = new DaoDemo();
        daoDemo.findAllUsersDemo();
        daoDemo.createUserDemo();
        daoDemo.findAllUsersDemo();
        daoDemo.findUserByEmailDemo("itsuprik@gmail.com");
        RoomClass rc = RoomClass.LUX;
        System.out.println(rc.toString());
    }

    private void findAllUsersDemo() {
        try (UserDao userDao = daoFactory.createUserDao()) {
            List<User> userList = userDao.findAll();
            System.out.println(userList);
        }
    }

    private void createUserDemo() {
        User mk = new User(UUID.randomUUID().hashCode(), "Maksym",
                "Konovalyuk", "mk2@mk_world.com", "Postavte70");
        try (UserDao userDao = daoFactory.createUserDao()) {
            userDao.create(mk);
        }
    }

    private void findUserByEmailDemo(String email) {
        try (UserDao userDao = daoFactory.createUserDao()) {
            User foundedUser = userDao.findByEmail(email);
            System.out.println(foundedUser);
        }
    }
}
