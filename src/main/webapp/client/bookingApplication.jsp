<%--
  Created by IntelliJ IDEA.
  User: itsuprun
  Date: 3/9/21
  Time: 10:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>My hotel.</h1>
<h2>Fill application form</h2>
<form action="application" method="post">
    <input name="numberOfBeds" type="text" placeholder="Input number of beds:" required><br>
    <input list="roomclasses" name="roomClass" placeholder="Choose room class" required><br>
    <datalist id="roomclasses">
        <c:forEach var="roomClass" items="${roomClassesList}">
            <option value="${roomClass.toString()}"></option>
        </c:forEach>
    </datalist>
    <h4>Choose start booking date:</h4>
    <input name="bookingDateStart" type="date" min="2021-01-01" required><br>
    <h4>Choose end booking date:</h4>
    <input name="bookingDateEnd" type="date" min="2021-01-01" required><br>
    <input type="submit" value="submit">
    ${messageFromServer}
</form>
</body>
</html>
